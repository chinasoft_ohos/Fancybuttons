package com.example.fcbtn;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.window.dialog.ToastDialog;

public class XmlAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_only_text).setClickedListener(component -> new ToastDialog(getContext()).setText("Button Clicked").show());

    }



}
