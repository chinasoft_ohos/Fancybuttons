package com.example.fcbtn;


import mehdi.sakout.fancybuttons.FancyButton;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.utils.Color;

import static mehdi.sakout.fancybuttons.FancyButton.POSITION_LEFT;
import static ohos.agp.utils.LayoutAlignment.CENTER;

public class CodeAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_code);
        DirectionalLayout aa = (DirectionalLayout) findComponentById(ResourceTable.Id_aa);
        FancyButton facebookLoginBtn = new FancyButton(this);
        facebookLoginBtn.setText("Login with Facebook");
        facebookLoginBtn.setBackgroundColor(new Color(Color.getIntColor("#3b5998")));
        facebookLoginBtn.setFocusBackgroundColor(new Color(Color.getIntColor("#5474b8")));
        facebookLoginBtn.setRadius(2);
        facebookLoginBtn.setIconPadding(0, 10, 10, 10);
        facebookLoginBtn.setTextGravity(CENTER);
        facebookLoginBtn.setIconResource(ResourceTable.Media_xi_f);
        facebookLoginBtn.setIconPosition(POSITION_LEFT);
        facebookLoginBtn.setFontIconSize(2f);
        facebookLoginBtn.setTextSize(15);


        FancyButton foursquareBtn = new FancyButton(this);
        foursquareBtn.setText("Check in");
        foursquareBtn.setBackgroundColor(new Color(Color.getIntColor("#0072b1")));
        foursquareBtn.setFocusBackgroundColor(new Color(Color.getIntColor("#228fcb")));
        foursquareBtn.setRadius(2);
        foursquareBtn.setIconPadding(0, 3, 0, 0);
        foursquareBtn.setTextGravity(CENTER);
        foursquareBtn.setIconResource(ResourceTable.Media_cu_f);
        foursquareBtn.setIconPosition(FancyButton.POSITION_TOP);
        foursquareBtn.setFontIconSize(2f);
        foursquareBtn.setTextSize(15);


        FancyButton installBtn = new FancyButton(this);
        installBtn.setText("Google play install");
        installBtn.setBackgroundColor(new Color(Color.getIntColor("#a4c639")));
        installBtn.setFocusBackgroundColor(new Color(Color.getIntColor("#bfe156")));
        installBtn.setRadius(2);
        installBtn.setBorderWidth(0);
        installBtn.setIconPadding(0, 0, 0, 0);
        installBtn.setTextSize(15);
        installBtn.setEnabled(false);


//
        FancyButton signupBtn = new FancyButton(this);
        DirectionalLayout.LayoutConfig layoutParams2 = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        layoutParams2.setMargins(30, 10, 30, 10);
        signupBtn.setLayoutConfig(layoutParams2);
        signupBtn.setText("Repost the song");
        signupBtn.setFontIconSize(2f);
        signupBtn.setTextGravity(CENTER);
        signupBtn.setIconResource(ResourceTable.Media_soundcloud);
        signupBtn.setBackgroundColor(new Color(Color.getIntColor("#ff8800")));
        signupBtn.setFocusBackgroundColor(new Color(Color.getIntColor("#ffa43c")));
        signupBtn.setTextSize(15);
        signupBtn.setIconPadding(10, 10, 30, 10);

        DirectionalLayout.LayoutConfig layoutParams = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        layoutParams.setMargins(30, 20, 30, 20);

        aa.addComponent(facebookLoginBtn, layoutParams);
        aa.addComponent(foursquareBtn, layoutParams);
        aa.addComponent(installBtn, layoutParams);
        aa.addComponent(signupBtn);
    }


}
