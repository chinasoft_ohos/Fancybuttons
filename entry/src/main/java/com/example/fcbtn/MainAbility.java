package com.example.fcbtn;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_index);
        Text aa = (Text) findComponentById(ResourceTable.Id_a);
        Text bb = (Text) findComponentById(ResourceTable.Id_b);
        aa.setClickedListener(component -> {
            Intent intet = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName("com.example.fcbtn.XmlAbility")
                    .build();
            intet.setOperation(operation);
            startAbility(intet);
        });

        bb.setClickedListener(component -> {
            Intent intet = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName("com.example.fcbtn.CodeAbility")
                    .build();
            intet.setOperation(operation);
            startAbility(intet);
        });

    }
}
