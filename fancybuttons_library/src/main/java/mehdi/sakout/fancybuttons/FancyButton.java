package mehdi.sakout.fancybuttons;

import mehdi.sakout.fancybuttons.utils.DensityUtils;
import mehdi.sakout.fancybuttons.utils.TypedAttrUtils;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.global.resource.NotExistException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import static ohos.agp.components.ComponentState.*;
import static ohos.agp.utils.LayoutAlignment.CENTER;

public class FancyButton extends DirectionalLayout {
    private Context mContext;
    private Color mDefaultBackgroundColor = Color.BLACK;
    private Color mFocusBackgroundColor = Color.WHITE;
    private Color mDisabledBackgroundColor = new Color(Color.getIntColor("#f6f7f9"));
    private Color mDisabledTextColor = new Color(Color.getIntColor("#bec2c9"));
    private Color mDisabledBorderColor = new Color(Color.getIntColor("#dddfe2"));

    // Text Attributes
    private Color mDefaultTextColor = Color.WHITE;

    private static final int NUM_10 = 10;
    private static final int NUM_1 = 1;
    private static final int NUM_2 = 2;
    private static final int NUM_3 = 3;
    private static final int NUM_4 = 4;
    private static final int NUM_5 = 5;
    private static final int NUM_15 = 15;
    private static final int NUM_20 = 20;

    private int mDefaultTextSize = NUM_15;
    private int mDefaultTextGravity = TextAlignment.CENTER; // Gravity.CENTER
    private String mStrDefaultTextGravity = "center"; // Gravity.CENTER
    private String mText = "";

    // # Icon Attributes
    private Element mIconResource = null;
    private int mIconResourceId = 0;

    private Element enableIconResource = null;
    private int enableIconResourceId = 0;
    private int mFontIconSize = DensityUtils.fpToPx(getContext(), NUM_15);
    private int mIconPosition = NUM_1;

    private int mIconPaddingLeft = NUM_10;
    private int mIconPaddingRight = NUM_10;
    private int mIconPaddingTop = 0;
    private int mIconPaddingBottom = 0;

    private int textPaddingLeft = 0;
    private int textPaddingRight = 0;
    private int textPaddingTop = 0;
    private int textPaddingBottom = 0;
    private Color mBorderColor = Color.TRANSPARENT;
    private int mBorderWidth = NUM_10;
    private int mRadius = 0;
    private int mRadiusTopLeft = 0;
    private int mRadiusTopRight = 0;
    private int mRadiusBottomLeft = 0;
    private int mRadiusBottomRight = 0;
    private boolean isBold = false;

    /**
     * setFontIconSize
     *
     * @param iconSize iconSize
     */
    public void setFontIconSize(float iconSize) {
        this.iconSize = iconSize;

        initializeFancyButton();
    }

    private float iconSize = 1;

    private boolean isEnabled = true;

    private boolean isTextAllCaps = false;

    private Font mTextTypeFace = null;


    /**
     * Tags to identify icon position
     */
    public static final int POSITION_LEFT = NUM_1;
    public static final int POSITION_RIGHT = NUM_2;
    public static final int POSITION_TOP = NUM_3;
    public static final int POSITION_BOTTOM = NUM_4;

    private Image mIconView;
    private Text mFontIconView;
    private Text mTextView;

    private boolean isGhost = false; // Default is a solid button !
    private boolean isUseSystemFont = false; // Default is using robotoregular.ttf

    private static final String TEXT_BOTTOM = "BOTTOM";
    private static final String TEXT_CENTER = "CENTER";
    private static final String TEXT_END = "END";
    private static final String TEXT_HORIZONTAL_CENTER = "HORIZONTAL_CENTER";
    private static final String TEXT_LEFT = "LEFT";
    private static final String TEXT_RIGHT = "RIGHT";
    private static final String TEXT_START = "START";
    private static final String TEXT_TOP = "TOP";
    private static final String TEXT_VERTICAL_CENTER = "VERTICAL_CENTER";
    private int margin5;

    /**
     * 构造
     *
     * @param context context
     */
    public FancyButton(Context context) {
        super(context);
        this.mContext = context;
        mTextTypeFace = Font.DEFAULT;
        initializeFancyButton();
    }


    /**
     * 构造
     *
     * @param context 构造
     * @param attrSet attrSet
     */
    public FancyButton(Context context, AttrSet attrSet) {
        super(context, attrSet);
        this.mContext = context;
        mTextTypeFace = Font.DEFAULT;
        init(attrSet);
        initializeFancyButton();
    }

    private void initializeFancyButton() {
        initializeButtonContainer();
        mTextView = setupTextView();
        mIconView = setupIconView();

        removeAllComponents();
        setupBackground();
        ArrayList<Component> components = new ArrayList<>();
        if (mIconPosition == POSITION_LEFT || mIconPosition == POSITION_TOP) {
            if (mIconView != null) {
                components.add(mIconView);
            }
            if (mTextView != null) {
                components.add(mTextView);
            }
        } else {
            if (mTextView != null) {
                components.add(mTextView);
            }
            if (mIconView != null) {
                components.add(mIconView);
            }
        }
        for (Component component : components) {
            this.addComponent(component);
        }
    }

    /**
     * setupBackground
     */
    private void setupBackground() {
        // Default Drawable
        ShapeElement defaultDrawable = new ShapeElement();
        applyRadius(defaultDrawable);

        //Focus Drawable
        ShapeElement focusDrawable = new ShapeElement();
        applyRadius(focusDrawable);

        // Disabled Drawable
        ShapeElement disabledDrawable = new ShapeElement();
        applyRadius(disabledDrawable);

        // 默认背景
        if (isGhost) {
            defaultDrawable.setRgbColor(new RgbColor(0)); // Hollow Background
        } else {
            defaultDrawable.setRgbColor(getRgbColor(mDefaultBackgroundColor));
        }

        focusDrawable.setRgbColor(getRgbColor(mFocusBackgroundColor));
        disabledDrawable.setRgbColor(getRgbColor(mDisabledBackgroundColor));
        disabledDrawable.setStroke(mBorderWidth, getRgbColor(mDisabledBorderColor));

        // Handle Border
        if (mBorderColor.getValue() != 0) {
            defaultDrawable.setStroke(mBorderWidth, getRgbColor(mBorderColor));
        }

        // Handle disabled border color
        if (!isEnabled) {

            //透明
            defaultDrawable.setStroke(mBorderWidth, getRgbColor(mDisabledBorderColor));
            if (isGhost) {
                disabledDrawable.setRgbColor(new RgbColor(0));
            }
        }

        StateElement states = new StateElement();
        // Focus/Pressed Drawable
        ShapeElement drawable2 = new ShapeElement();
        applyRadius(drawable2);

        if (isGhost) {
            drawable2.setRgbColor(new RgbColor(0)); // No focus color
        } else {
            drawable2.setRgbColor(getRgbColor(mFocusBackgroundColor));
        }

        // Handle Button Border
        if (mBorderColor.getValue() != 0) {
            if (isGhost) {
                drawable2.setStroke(mBorderWidth, getRgbColor(mFocusBackgroundColor)); // Border is the main part of button now
            } else {
                drawable2.setStroke(mBorderWidth, getRgbColor(mBorderColor));
            }
        }

        if (!isEnabled) {
            drawable2.setStroke(mBorderWidth, getRgbColor(mDisabledBorderColor));
//            if (isGhost) {
//                drawable2.setStroke(mBorderWidth, getRgbColor(mDisabledBorderColor));
//            } else {
//                drawable2.setStroke(mBorderWidth, getRgbColor(mDisabledBorderColor));
//            }
        }

        if (mFocusBackgroundColor != null) {
            states.addState(new int[]{COMPONENT_STATE_PRESSED}, drawable2);
            states.addState(new int[]{COMPONENT_STATE_FOCUSED}, drawable2);
            states.addState(new int[]{COMPONENT_STATE_DISABLED}, disabledDrawable);
        }

        states.addState(new int[]{}, defaultDrawable);
        this.setBackground(states);
    }

    /**
     * getRgbColor
     *
     * @param color color
     * @return RgbColor
     */
    private RgbColor getRgbColor(Color color) {
        return RgbColor.fromArgbInt(color.getValue());
    }

    /**
     * getRgbColor
     *
     * @param color color
     * @return RgbColor
     */
    public RgbColor getRgbColor(int color) {
        return RgbColor.fromArgbInt(color);
    }

    private void applyRadius(ShapeElement defaultDrawable) {
        mRadiusBottomRight = DensityUtils.fpToPx(getContext(), mRadiusBottomRight);
        mRadiusBottomLeft = DensityUtils.fpToPx(getContext(), mRadiusBottomLeft);
        mRadiusTopRight = DensityUtils.fpToPx(getContext(), mRadiusTopRight);
        mRadiusTopLeft = DensityUtils.fpToPx(getContext(), mRadiusTopLeft);
        if (mRadius > 0) {
            defaultDrawable.setCornerRadius(DensityUtils.fpToPx(getContext(), mRadius));
        } else {
            defaultDrawable.setCornerRadiiArray(new float[]{mRadiusTopLeft, mRadiusTopLeft, mRadiusTopRight, mRadiusTopRight,
                    mRadiusBottomRight, mRadiusBottomRight, mRadiusBottomLeft, mRadiusBottomLeft});
        }
    }

    /**
     * Text Icon resource component
     *
     * @return img
     */
    private Image setupIconView() {
        Image img = new Image(mContext);

        if (mIconResource != null || mIconResourceId != 0 || enableIconResourceId != 0 || enableIconResource != null) {
            img.setScale(iconSize, iconSize);
            if (!isEnabled) {
                // 判断 iconRescource是否为空
                if (enableIconResource != null) {
                    img.setImageElement(enableIconResource);
                    // iconRescource为空时  判断enableIconResourceId的方式
                } else if (enableIconResourceId != 0) {
                    img.setPixelMap(mIconResourceId);
                }
            } else {
                if (mIconResource != null) {
                    img.setImageElement(mIconResource);
                } else {
                    img.setPixelMap(mIconResourceId);
                }
            }
            img.setPadding(mIconPaddingLeft, mIconPaddingTop, mIconPaddingRight, mIconPaddingBottom);
            LayoutConfig imgParams = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
            if (mTextView != null) {
                margin5 = DensityUtils.fpToPx(getContext(), NUM_5);
                imgParams.alignment = CENTER;
                imgParams.setMarginsTopAndBottom(margin5, margin5);
                imgParams.setMarginsLeftAndRight(margin5, margin5);
            } else {
                imgParams.alignment = LayoutAlignment.VERTICAL_CENTER;
            }
            img.setLayoutConfig(imgParams);

            return img;
        }
        return null;
    }

    private Text setupTextView() {
        Text textView = new Text(mContext);
        textView.setText(mText);
        if (isBold) {
            textView.setFont(Font.DEFAULT_BOLD);
        }
        textPaddingLeft = DensityUtils.fpToPx(getContext(), textPaddingLeft);
        textPaddingTop = DensityUtils.fpToPx(getContext(), textPaddingTop);
        textPaddingRight = DensityUtils.fpToPx(getContext(), textPaddingRight);
        textPaddingBottom = DensityUtils.fpToPx(getContext(), textPaddingBottom);
        textView.setPadding(textPaddingLeft, textPaddingTop, textPaddingRight, textPaddingBottom);
        // 自动换行
        textView.setMultipleLine(true);
        textView.setTextAlignment(TextAlignment.CENTER);
        textView.setTextColor(isEnabled ? mDefaultTextColor : mDisabledTextColor);
        textView.setTextSize(DensityUtils.fpToPx(getContext(), mDefaultTextSize));
        textView.setLayoutConfig(new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT));
        return textView;
    }

    /**
     * 各种属性初始化
     *
     * @param attrSet
     */
    private void init(AttrSet attrSet) {
        // 背景颜色与边框初始化
        initBgColor(attrSet);
        initTextAttr(attrSet);
        initIconAttr(attrSet);
        initBorderAttr(attrSet);
        initFontAttr(attrSet);
        initDrawable(attrSet);
        if (mText != null)
            mText = isTextAllCaps ? mText.toUpperCase() : mText;
    }

    /**
     * 获取drawable资源
     *
     * @param attrSet 资源
     * @throws IOException IOException
     * @throws NotExistException NotExistException
     */
    private void initDrawable(AttrSet attrSet) {
        mIconResource = TypedAttrUtils.getDrawable(attrSet, "fb_iconResource", null);
        enableIconResource = TypedAttrUtils.getDrawable(attrSet, "fb_enableIconResource", null);
    }

    /**
     * 文字
     *
     * @param attrSet 资源
     */
    private void initFontAttr(AttrSet attrSet) {
        isEnabled = TypedAttrUtils.getBoolean(attrSet, "fb_enabled", isEnabled);
        super.setEnabled(isEnabled);
        isTextAllCaps = TypedAttrUtils.getBoolean(attrSet, "fb_textAllCaps", isTextAllCaps);
        isGhost = TypedAttrUtils.getBoolean(attrSet, "fb_ghost", isGhost);
        isUseSystemFont = TypedAttrUtils.getBoolean(attrSet, "fb_useSystemFont", isUseSystemFont);
    }

    /**
     * 边框
     *
     * @param attrSet 资源
     */
    private void initBorderAttr(AttrSet attrSet) {

        mDisabledBorderColor = TypedAttrUtils.getColor(attrSet, "fb_disabledBorderColor", mDisabledBorderColor);
        mBorderColor = TypedAttrUtils.getColor(attrSet, "fb_borderColor", mBorderColor);
        mBorderWidth = TypedAttrUtils.getDimensionPixelSize(attrSet, "fb_borderWidth", mBorderWidth);
        mBorderWidth = DensityUtils.fpToPx(getContext(), mBorderWidth);
        mRadius = TypedAttrUtils.getDimensionPixelSize(attrSet, "fb_radius", mRadius);
        mRadiusTopLeft = TypedAttrUtils.getDimensionPixelSize(attrSet, "fb_radiusTopLeft", mRadiusTopLeft);
        mRadiusTopRight = TypedAttrUtils.getDimensionPixelSize(attrSet, "fb_radiusTopRight", mRadiusTopRight);
        mRadiusBottomLeft = TypedAttrUtils.getDimensionPixelSize(attrSet, "fb_radiusBottomLeft", mRadiusBottomLeft);
        mRadiusBottomRight = TypedAttrUtils.getDimensionPixelSize(attrSet, "fb_radiusBottomRight", mRadiusBottomRight);
    }

    /**
     * icon
     *
     * @param attrSet 资源
     */
    private void initIconAttr(AttrSet attrSet) {
        // icon
        mFontIconSize = TypedAttrUtils.getDimensionPixelSize(attrSet, "fb_fontIconSize", mFontIconSize);
        mIconPosition = TypedAttrUtils.getDimensionPixelSize(attrSet, "fb_iconPosition", mIconPosition);
        iconSize = TypedAttrUtils.getFloatSize(attrSet, "fb_iconSize", iconSize);

        mIconPaddingLeft = TypedAttrUtils.getDimensionPixelSize(attrSet, "fb_iconPaddingLeft", mIconPaddingLeft);
        mIconPaddingTop = TypedAttrUtils.getDimensionPixelSize(attrSet, "fb_iconPaddingTop", mIconPaddingTop);
        mIconPaddingRight = TypedAttrUtils.getDimensionPixelSize(attrSet, "fb_iconPaddingRight", mIconPaddingRight);
        mIconPaddingBottom = TypedAttrUtils.getDimensionPixelSize(attrSet, "fb_iconPaddingBottom", mIconPaddingBottom);

        textPaddingLeft = TypedAttrUtils.getDimensionPixelSize(attrSet, "fb_textPaddingLeft", textPaddingLeft);
        textPaddingRight = TypedAttrUtils.getDimensionPixelSize(attrSet, "fb_textPaddingRight", textPaddingRight);
        textPaddingTop = TypedAttrUtils.getDimensionPixelSize(attrSet, "fb_textPaddingTop", textPaddingTop);
        textPaddingBottom = TypedAttrUtils.getDimensionPixelSize(attrSet, "fb_textPaddingBottom", textPaddingBottom);
    }

    /**
     * 是否加粗
     *
     * @return true 是 false 否
     */
    public boolean isBold() {
        return isBold;
    }

    /**
     * 设置加粗
     *
     * @param isBold 是否加粗
     */
    public void setBold(boolean isBold) {
        this.isBold = isBold;
        initializeFancyButton();
    }

    /**
     * text
     *
     * @param attrSet 资源
     */

    private void initTextAttr(AttrSet attrSet) {
        // # Text 属性
        mDisabledTextColor = TypedAttrUtils.getColor(attrSet, "fb_disabledTextColor", mDisabledTextColor);
        mDefaultTextColor = TypedAttrUtils.getColor(attrSet, "fb_textColor", mDefaultTextColor);
        //默认文字大小
        mDefaultTextSize = TypedAttrUtils.getDimensionPixelSize(attrSet, "fb_textSize", mDefaultTextSize);
        // 取出来的是string 通过相关判断转化成int类型以供使用
        mStrDefaultTextGravity = TypedAttrUtils.getString(attrSet, "fb_textGravity", mStrDefaultTextGravity);
        String blod = TypedAttrUtils.getString(attrSet, "fb_isBold", mStrDefaultTextGravity);
        isBold = blod.toLowerCase(Locale.ROOT).equals("true");
        setgravityStyle();

        // 默认内容
        mText = TypedAttrUtils.getString(attrSet, "fb_text", "");
    }

    private void setgravityStyle() {
        // 判断是否为空
        if (!mStrDefaultTextGravity.isEmpty()) {
            if (TEXT_BOTTOM.equals(mStrDefaultTextGravity.toUpperCase(Locale.ROOT))) {
                mDefaultTextGravity = LayoutAlignment.BOTTOM;
            } else if (TEXT_CENTER.equals(mStrDefaultTextGravity.toUpperCase(Locale.ROOT))) {
                mDefaultTextGravity = LayoutAlignment.CENTER;
            } else if (TEXT_END.equals(mStrDefaultTextGravity.toUpperCase(Locale.ROOT))) {
                mDefaultTextGravity = LayoutAlignment.END;
            } else if (TEXT_HORIZONTAL_CENTER.equals(mStrDefaultTextGravity.toUpperCase(Locale.ROOT))) {
                mDefaultTextGravity = LayoutAlignment.HORIZONTAL_CENTER;
            } else if (TEXT_LEFT.equals(mStrDefaultTextGravity.toUpperCase(Locale.ROOT))) {
                mDefaultTextGravity = LayoutAlignment.LEFT;
            } else if (TEXT_RIGHT.equals(mStrDefaultTextGravity.toUpperCase(Locale.ROOT))) {
                mDefaultTextGravity = LayoutAlignment.RIGHT;
            } else if (TEXT_START.equals(mStrDefaultTextGravity.toUpperCase(Locale.ROOT))) {
                mDefaultTextGravity = LayoutAlignment.START;
            } else if (TEXT_TOP.equals(mStrDefaultTextGravity.toUpperCase(Locale.ROOT))) {
                mDefaultTextGravity = LayoutAlignment.TOP;
            } else if (TEXT_VERTICAL_CENTER.equals(mStrDefaultTextGravity.toUpperCase())) {
                mDefaultTextGravity = LayoutAlignment.VERTICAL_CENTER;
            }
        }
    }


    /**
     * 背景
     *
     * @param attrSet 资源
     */
    private void initBgColor(AttrSet attrSet) {
        // Background 属性
        mDefaultBackgroundColor = TypedAttrUtils.getColor(attrSet, "fb_defaultColor", mDefaultBackgroundColor);
        mDisabledBackgroundColor = TypedAttrUtils.getColor(attrSet, "fb_disabledColor", mDisabledBackgroundColor);
        mFocusBackgroundColor = TypedAttrUtils.getColor(attrSet, "fb_focusColor", mFocusBackgroundColor);
    }


    /**
     * Initialize button container
     */
    private void initializeButtonContainer() {

        if (mIconPosition == POSITION_TOP || mIconPosition == POSITION_BOTTOM) {
            this.setOrientation(DirectionalLayout.VERTICAL);
        } else {
            this.setOrientation(DirectionalLayout.HORIZONTAL);
        }

        if (this.getLayoutConfig() == null) {
            LayoutConfig containerParams = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
            this.setLayoutConfig(containerParams);
        }
        // 文字居中
        this.setAlignment(mDefaultTextGravity | CENTER);

        if ((mIconResource == null || mIconResourceId != 0 || enableIconResourceId != 0 || enableIconResource != null) && getPaddingLeft() == 0 && getPaddingRight() == 0 && getPaddingTop() == 0 && getPaddingBottom() == 0) {

            this.setPadding(NUM_20, 0, NUM_20, 0);
        }
    }

    /**
     * Set Background color of the button
     *
     * @param mDefaultBackgroundColor 默认背景颜色
     */

    public void setBackgroundColor(Color mDefaultBackgroundColor) {
        this.mDefaultBackgroundColor = mDefaultBackgroundColor;
        if (mIconView != null || mTextView != null) {
            this.setupBackground();
        }
    }

    /**
     * Set Focus color of the button
     *
     * @param mFocusBackgroundColor 焦点颜色
     */
    public void setFocusBackgroundColor(Color mFocusBackgroundColor) {
        this.mFocusBackgroundColor = mFocusBackgroundColor;
        if (mIconView != null || mTextView != null) {
            setupBackground();
        }

    }

    /**
     * Set Disabled state color of the button
     *
     * @param mDisabledBackgroundColor 禁用状态颜色
     */
    public void setDisableBackgroundColor(Color mDisabledBackgroundColor) {
        this.mDisabledBackgroundColor = mDisabledBackgroundColor;
        if (mIconView != null || mTextView != null) {
            this.setupBackground();
        }
    }

    /**
     * Set Disabled state color of the button text
     *
     * @param mDisabledTextColor 禁用状态文字颜色
     */
    public void setDisableTextColor(Color mDisabledTextColor) {
        this.mDisabledTextColor = mDisabledTextColor;
        if (mTextView == null)
            initializeFancyButton();
        else if (!isEnabled)
            mTextView.setTextColor(mDisabledTextColor);
    }

    /**
     * Set Disabled state color of the button border
     *
     * @param mDisabledBorderColor 禁用状态边框颜色
     */
    public void setDisableBorderColor(Color mDisabledBorderColor) {
        this.mDisabledBorderColor = mDisabledBorderColor;
        if (mIconView != null || mTextView != null) {
            setupBackground();
        }
    }

    /**
     * Set the color of text
     *
     * @param mDefaultTextColor 默认文字颜色
     */
    public void setTextColor(Color mDefaultTextColor) {
        this.mDefaultTextColor = mDefaultTextColor;
        if (mTextView == null)
            initializeFancyButton();
        else
            mTextView.setTextColor(mDefaultTextColor);
    }


    /**
     * Set the size of Text in sp
     *
     * @param textSize 文字大小
     */
    public void setTextSize(int textSize) {
        this.mDefaultTextSize = textSize;
        if (mTextView != null) {
            mTextView.setTextSize(mDefaultTextSize);
        }
        initializeFancyButton();
    }

    /**
     * Set the gravity of Text
     *
     * @param mDefaultTextGravity 默认文字方向
     */
    public void setTextGravity(int mDefaultTextGravity) {
        this.mDefaultTextGravity = mDefaultTextGravity;
        if (mTextView != null) {
            this.setAlignment(mDefaultTextGravity);
        }
    }


    /**
     * Set an icon from resources to the button
     *
     * @param mIconResource 将资源图标设置为按钮
     */
    public void setIconResource(int mIconResource) {
        this.mIconResourceId = mIconResource;
        if (mIconView == null) {
            mFontIconView = null;
            initializeFancyButton();
        } else {
            if (!isEnabled) {
                // 不可点击 什么都不做
                return;
            } else {
                mIconView.setPixelMap(mIconResourceId);
            }
        }
    }

    /**
     * 将可绘制对象设置为按钮
     *
     * @param mIconResource Element
     */
    public void setIconResource(Element mIconResource) {
        this.mIconResource = mIconResource;
        if (mIconView == null) {
            mFontIconView = null;
            initializeFancyButton();
        } else {
            if (!isEnabled) {
                // 不可点击 什么都不做
                return;
            } else {
                mIconView.setImageElement(mIconResource);
            }
        }
    }

    /**
     * setEnableIconResource
     *
     * @param enableIconResourceId enableIconResourceId
     */
    public void setEnableIconResource(int enableIconResourceId) {
        this.enableIconResourceId = enableIconResourceId;

        if (mIconView == null || mFontIconView != null) {
            mFontIconView = null;
            initializeFancyButton();
        } else {
            if (!isEnabled) {
                // 不可点击状态  使用灰色图片
                if (enableIconResourceId != 0) {
                    // 用id的方式添加
                    mIconView.setPixelMap(enableIconResourceId);
                }
            }
        }
    }

    /**
     * setIconPosition
     *
     * @param position position
     */
    public void setIconPosition(int position) {

        if (position > 0 && position < 5) {
            this.mIconPosition = position;
        } else {
            mIconPosition = POSITION_LEFT;
        }
        initializeFancyButton();
    }

    /**
     * Set Padding for mIconView and mFontIconSize
     *
     * @param paddingLeft   : Padding Left
     * @param paddingTop    : Padding Top
     * @param paddingRight  : Padding Right
     * @param paddingBottom : Padding Bottom
     */
    public void setIconPadding(int paddingLeft, int paddingTop, int paddingRight, int paddingBottom) {
        this.mIconPaddingLeft = paddingLeft;
        this.mIconPaddingTop = paddingTop;
        this.mIconPaddingRight = paddingRight;
        this.mIconPaddingBottom = paddingBottom;
        if (mIconView != null) {
            mIconView.setPadding(this.mIconPaddingLeft, this.mIconPaddingTop, this.mIconPaddingRight, this.mIconPaddingBottom);
        }
        if (mFontIconView != null) {
            mFontIconView.setPadding(this.mIconPaddingLeft, this.mIconPaddingTop, this.mIconPaddingRight, this.mIconPaddingBottom);
        }
    }

    /**
     * getmIconPaddingLeft
     *
     * @return mIconPaddingLeft
     */
    public int getmIconPaddingLeft() {
        return mIconPaddingLeft;
    }

    /**
     * setmIconPaddingLeft
     *
     * @param mIconPaddingLeft mIconPaddingLeft
     */
    public void setmIconPaddingLeft(int mIconPaddingLeft) {
        this.mIconPaddingLeft = mIconPaddingLeft;
    }

    /**
     * getmIconPaddingRight
     *
     * @return mIconPaddingRight
     */
    public int getmIconPaddingRight() {
        return mIconPaddingRight;
    }

    /**
     * setmIconPaddingRight
     *
     * @param mIconPaddingRight mIconPaddingRight
     */
    public void setmIconPaddingRight(int mIconPaddingRight) {
        this.mIconPaddingRight = mIconPaddingRight;
    }

    /**
     * getmIconPaddingTop
     *
     * @return mIconPaddingTop
     */
    public int getmIconPaddingTop() {
        return mIconPaddingTop;
    }

    /**
     * setIconPaddingTop
     *
     * @param mIconPaddingTop mIconPaddingTop
     */
    public void setIconPaddingTop(int mIconPaddingTop) {
        this.mIconPaddingTop = mIconPaddingTop;
    }

    /**
     * getIconPaddingBottom
     *
     * @return mIconPaddingBottom
     */
    public int getIconPaddingBottom() {
        return mIconPaddingBottom;
    }

    /**
     * setIconPaddingBottom
     *
     * @param mIconPaddingBottom mIconPaddingBottom
     */
    public void setIconPaddingBottom(int mIconPaddingBottom) {
        this.mIconPaddingBottom = mIconPaddingBottom;
    }

    /**
     * setBorderColor
     *
     * @return Color
     */
    public Color setBorderColor() {
        return mBorderColor;
    }

    /**
     * setmBorderColor
     *
     * @param mBorderColor mBorderColor
     */
    public void setmBorderColor(Color mBorderColor) {
        this.mBorderColor = mBorderColor;
        if (mIconView != null || mTextView != null) {
            this.setupBackground();
        }
    }

    /**
     * setBorderWidth
     *
     * @param mBorderWidth mBorderWidth
     */
    public void setBorderWidth(int mBorderWidth) {
        this.mBorderWidth = mBorderWidth;
        if (mIconView != null || mTextView != null) {
            this.setupBackground();
        }
    }

    /**
     * setRadius
     *
     * @param mRadius mRadius
     */
    public void setRadius(int mRadius) {
        this.mRadius = mRadius;
        if (mIconView != null || mTextView != null) {
            this.setupBackground();
        }
    }

    /**
     * setRadius radius
     *
     * @param radius 半径
     */
    public void setRadius(int[] radius) {
        this.mRadiusTopLeft = radius[0];
        this.mRadiusTopRight = radius[1];
        this.mRadiusBottomLeft = radius[2];
        this.mRadiusBottomRight = radius[3];

        if (mIconView != null || mTextView != null) {
            this.setupBackground();
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        this.isEnabled = enabled;
        initializeFancyButton();
    }

    /**
     * mRadiusTopLeft
     *
     * @return mRadiusTopLeft
     */
    public int getmRadiusTopLeft() {
        return mRadiusTopLeft;
    }

    /**
     * setmRadiusTopLeft
     *
     * @param mRadiusTopLeft mRadiusTopLeft
     */
    public void setmRadiusTopLeft(int mRadiusTopLeft) {
        this.mRadiusTopLeft = mRadiusTopLeft;
    }

    /**
     * getmRadiusTopRight
     *
     * @return mRadiusTopRight
     */
    public int getmRadiusTopRight() {
        return mRadiusTopRight;
    }

    /**
     * setmRadiusTopRight
     *
     * @param mRadiusTopRight mRadiusTopRight
     */
    public void setmRadiusTopRight(int mRadiusTopRight) {
        this.mRadiusTopRight = mRadiusTopRight;
    }

    /**
     * getmRadiusBottomLeft
     *
     * @return mRadiusBottomLeft
     */
    public int getmRadiusBottomLeft() {
        return mRadiusBottomLeft;
    }

    /**
     * setmRadiusBottomLeft
     *
     * @param mRadiusBottomLeft mRadiusBottomLeft
     */
    public void setmRadiusBottomLeft(int mRadiusBottomLeft) {
        this.mRadiusBottomLeft = mRadiusBottomLeft;
    }

    /**
     * getmRadiusBottomRight
     *
     * @return getmRadiusBottomRight
     */
    public int getmRadiusBottomRight() {
        return mRadiusBottomRight;
    }

    /**
     * setmRadiusBottommRight
     *
     * @param mRadiusBottomRight setmRadiusBottommRight
     */
    public void setmRadiusBottomRight(int mRadiusBottomRight) {
        this.mRadiusBottomRight = mRadiusBottomRight;
    }

    /**
     * ismTextAllCaps
     *
     * @return isTextAllCaps
     */
    public boolean ismTextAllCaps() {
        return isTextAllCaps;
    }

    /**
     * Set Text of the button
     *
     * @param text : Text
     */
    public void setText(String text) {
        text = isTextAllCaps ? text.toUpperCase(Locale.ROOT) : text;
        this.mText = text;
        if (mTextView == null) {
            initializeFancyButton();
        } else {
            mTextView.setText(text);
        }

    }

    /**
     * Set the capitalization of text
     *
     * @param isTextAllCaps : is text to be capitalized
     */
    public void setTextAllCaps(boolean isTextAllCaps) {
        this.isTextAllCaps = isTextAllCaps;
        setText(mText);
    }

    /**
     * getmTextTypeFace
     *
     * @return mTextTypeFace
     */
    public Font getmTextTypeFace() {
        return mTextTypeFace;
    }

    /**
     * setTextTypeFace
     *
     * @param mTextTypeFace mTextTypeFace
     */
    public void setTextTypeFace(Font mTextTypeFace) {
        this.mTextTypeFace = mTextTypeFace;
    }

    /**
     * PositionRight
     *
     * @return PositionLeft
     */
    public static int getPositionLeft() {
        return POSITION_LEFT;
    }

    /**
     * getPositionRight
     *
     * @return PositionRight
     */
    public static int getPositionRight() {
        return POSITION_RIGHT;
    }

    /**
     * getPositionTop
     *
     * @return PositionTop
     */
    public static int getPositionTop() {
        return POSITION_TOP;
    }

    /**
     * getPositionBottom
     *
     * @return PositionBottom
     */
    public static int getPositionBottom() {
        return POSITION_BOTTOM;
    }

    /**
     * getmIconView
     *
     * @return mIconView
     */
    public Image getmIconView() {
        return mIconView;
    }

    /**
     * setIconView
     *
     * @param mIconView mIconView
     */
    public void setIconView(Image mIconView) {
        this.mIconView = mIconView;
    }

    /**
     * getFontIconView
     *
     * @return mFontIconView
     */
    public Text getFontIconView() {
        return mFontIconView;
    }

    /**
     * setFontIconView
     *
     * @param mFontIconView mFontIconView
     */
    public void setFontIconView(Text mFontIconView) {
        this.mFontIconView = mFontIconView;
    }

    /**
     * getTextView
     *
     * @return mTextView
     */
    public Text getTextView() {
        return mTextView;
    }

    /**
     * setTextViewv
     *
     * @param mTextView mTextView
     */
    public void setTextView(Text mTextView) {
        this.mTextView = mTextView;
    }

    /**
     * isGhost
     *
     * @return isGhost
     */
    public boolean isGhost() {
        return isGhost;
    }

    /**
     * setGhost
     *
     * @param isGhost isGhost
     */
    public void setGhost(boolean isGhost) {
        this.isGhost = isGhost;
        if (mIconView != null || mTextView != null) {
            this.setupBackground();
        }
    }

    /**
     * Return Text of the button
     *
     * @return Text
     */
    public CharSequence getText() {
        if (mTextView != null) {
            return mTextView.getText();
        } else {
            return "";
        }
    }

    /**
     * Return TextView Object of the FancyButton
     *
     * @return TextView Object
     */
    public Text getTextViewObject() {
        return mTextView;
    }

    /**
     * Return Icon of the FancyButton
     *
     * @return ImageView Object
     */
    public Image getIconImageObject() {
        return mIconView;
    }

    /**
     * setUseSystemFont
     *
     * @param isUseSystemFont isUseSystemFont
     */
    public void setUseSystemFont(boolean isUseSystemFont) {
        this.isUseSystemFont = isUseSystemFont;
    }
}
