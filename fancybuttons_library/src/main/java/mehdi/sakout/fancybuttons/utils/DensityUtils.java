package mehdi.sakout.fancybuttons.utils;

/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import ohos.agp.components.AttrHelper;
import ohos.agp.text.Font;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.io.File;
import java.util.Optional;

import static ohos.agp.components.AttrHelper.getDensity;

public class DensityUtils {
    /**
     * fp 转 Px
     *
     * @param context 上下文
     * @param fp fp
     * @return 返回值
     */
    public static int fpToPx(final Context context, final float fp) {
        return AttrHelper.fp2px(fp, getDensity(context));
    }

    /**
     * px 转 Fp
     *
     * @param context 上下文
     * @param px px
     * @return 返回值
     */
    public static int pxToFp(final Context context, final float px) {
        return Math.round(px * getDensity(context));
    }
    /**
     * 通过文件返回字体
     *
     * @param path 路径
     * @return 通过文件返回字体
     */
    public static Font getFilefont(File path) {
        Font.Builder font = new Font.Builder(path);
        return font.build();
    }

    /**
     * 通过name返回字体
     * Font font = DensityUtils.getFilefont(new File("resources/graphic/robotoregular.ttf"));
     * text.setFont(font)
     *
     * @param name name
     * @return 通过name返回字体
     */
    public static Font getStringfont(String name) {
        Font.Builder font = new Font.Builder(name);
        return font.build();
    }

    /**
     * 获取当前设备属性
     *
     * @param context 上下文
     * @return 返回值
     */
    public static Display getDeviceAttr(Context context) {
        Optional<Display> optional = DisplayManager.getInstance().getDefaultDisplay(context);
        return optional.get();
    }
}