# Fancybuttons

#### 项目介绍

- 项目名称：Fancybuttons
- 所属系列：openharmony的第三方组件适配移植
- 功能：可制作带icon、边框的按钮
- 项目移植状态：完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release v1.9.0

#### 效果演示
![输入图片说明](https://gitee.com/chinasoft_ohos/Fancybuttons/raw/master/img/demo.gif "fancybutton.gif")
#### 安装教程

1.在项目根目录下的build.gradle文件中，

 ```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
 2.在entry模块的build.gradle文件中，

  ```
  dependencies {
     implementation('com.gitee.chinasoft_ohos:Fancybuttons:1.0.0')
     ......
  }
  ```
在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件,
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下
#### 使用说明

1. 在根布局中写上FancyButtons的名称空间
```xml
xmlns:app="//schemas.huawei.com/apk/res/ohos"
```

  2.添加FancyButton布局

```xml
<mehdi.sakout.fancybuttons.FancyButton
    ohos:id="$+id:only_text"
    ohos:height="match_content"
    ohos:width="64vp"
    ohos:layout_alignment="center"
    app:fb_borderColor="#FF4D3121"
    app:fb_borderWidth="1"
    app:fb_defaultColor="#731010"
    app:fb_disabledTextColor="#011ffe"
    app:fb_focusColor="#FFFE9C01"
    app:fb_radius="2"
    app:fb_text="comments"
    app:fb_textColor="#ffffff"
    app:fb_textSize="14"/>
```

3.对应属性

```java
| XML属性               | Java属性           |描述  |
| fancy:fb_text        | setText(String)    |按钮文字 |
| fancy:fb_textColor   | setTextColor(int)  |按钮的文本颜色 |
| fancy:fb_textSize    | setTextSize(int)   |文本大小 |
| fancy:fb_textFont | setCustomTextFont(String)  |   字体家族的文本|
| fancy:fb_textGravity | setTextGravity(Int)      |    文字位置|
| fancy:fb_iconResource | setIconResource(Drawable)      | 绘制按钮的图标|
| fancy:fb_iconPosition | setIconPosition(int)      |   图标的位置:左右上下|
| fancy:fb_iconSize | setFontIconSize(int)      |   图标的大小 |
| fancy:fb_borderWidth | setBorderWidth(int)      |  边框宽度|
| fancy:fb_borderColor | setBorderColor(int)      |    边框颜色|
| fancy:fb_defaultColor | setBackgroundColor(int)      |    按钮的背景颜色|
| fancy:fb_focusColor | setFocusBackgroundColor(int)      |    按钮背景的焦点颜色|
| fancy:fb_disabledColor | setDisableBackgroundColor(int)      |   禁用按钮背景的颜色|
| fancy:fb_disabledTextColor | setDisableTextColor(int)      |    禁用按钮文本的颜色|
| fancy:fb_disabledBorderColor | setDisableBorderColor(int)      |   禁用按钮边框的颜色|
| fancy:fb_radius | setRadius(int)      |    Radius of the button|
| fancy:fb_radius(TopLeft, TopRight,BottomLeft,BottomRight) | setRadius(int[] radius)      |  为每个按钮角自定义半径|
| fancy:fb_iconPaddingLeft | setIconPadding(int,int,int,int)      |    向左填充|
| fancy:fb_iconPaddingRight | setIconPadding(int,int,int,int)      |    向右填充|
| fancy:fb_iconPaddingTop | setIconPadding(int,int,int,int)      |     向上填充|
| fancy:fb_iconPaddingBottom | setIconPadding(int,int,int,int)      |   向下填充|
| fancy:fb_ghost | setGhost(boolean)      |    Ghost (Hollow)|
| fancy:fb_useSystemFont | setUsingSystemFont(boolean) | If enabled, the button title will ignore its custom font and use the default system font |

Also you can use Attributes with default prefix (ohos:) which makes migrating of your project more fast.
Default Attributes have more priority than Attributes with prefix fancy.
```

4.默认属性

```xml
| ohos:enabled |
| ohos:text |
| ohos:textSize |
| ohos:textAllCaps |
```

5.支持的get方法

```java
| getText() | 返回按钮的文本值|
| getTextViewObject() | 返回textView对象|
| getIconImageObject() | 返回由iconResource定义的图标 |
```

6.Facebook Button使用

```java
FancyButton facebookLoginBtn = new FancyButton(this);
    facebookLoginBtn.setText("Login with Facebook");
    facebookLoginBtn.setBackgroundColor(new Color(Color.getIntColor("#3b5998")));
    facebookLoginBtn.setFocusBackgroundColor(new Color(Color.getIntColor("#5474b8")));
    facebookLoginBtn.setRadius(2);
    facebookLoginBtn.setIconPadding(0, 10, 10, 10);
    facebookLoginBtn.setTextGravity(CENTER);
    facebookLoginBtn.setIconResource(ResourceTable.Media_xi_f);
    facebookLoginBtn.setIconPosition(POSITION_LEFT);
    facebookLoginBtn.setFontIconSize(2f);
    facebookLoginBtn.setTextSize(15);
    

```
## 测试信息
```
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异
```
## 版本迭代

 - 1.0.0

## 版权和许可信息
```
MIT http://opensource.org/licenses/MIT
```